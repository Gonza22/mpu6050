#include "I2Cdev.h"
#include "MPU6050.h"
#include "Wire.h"

const int mpuAddress = 0x68;  //Direccion del mpu - Puede ser 0x68 o 0x69
MPU6050 mpu(mpuAddress);

int ax, ay, az;   //Variables de acelerometro
int gx, gy, gz;   //Variables de giroscopo

float A = 0.02;
float S[6] = {0, 0, 0, 0, 0, 0};

int pin_int = 2;  //Pin de interrupcion mpu6050


//Funcion de ajuste y muestra de datos
void printRAW()
{
  Serial.print(S[0]); Serial.print(",");
  Serial.print(S[1]); Serial.print(",");
  Serial.print(S[2]); Serial.print(",");
  Serial.print(S[3]); Serial.print(",");
  Serial.print(S[4]); Serial.print(",");
  Serial.print(S[5]); Serial.print(",");
  Serial.println("\t");
}

void setup()
{
  Serial.begin(2000000);
  Wire.begin();
  pinMode(pin_int, INPUT);
  mpu.initialize(); //Inicio el mpu y envio un mensaje segun a inicializacion sea correcta o no
  Serial.println(mpu.testConnection() ? F("IMU iniciado correctamente") : F("Error al iniciar IMU"));
}

void loop()
{

  
  //Si hay un dato de salida en el mpu6050
  if(digitalRead(pin_int) == LOW)
  {
    // Leer las aceleraciones y velocidades angulares
    mpu.getAcceleration(&ax, &ay, &az);
    mpu.getRotation(&gx, &gy, &gz);

    S[0] = (A * ax) + ((1 - A) * S[0]);
    S[1] = (A * ay) + ((1 - A) * S[1]);
    S[2] = (A * az) + ((1 - A) * S[2]);
    S[3] = (A * gx) + ((1 - A) * S[3]);
    S[4] = (A * gy) + ((1 - A) * S[4]);
    S[5] = (A * gz) + ((1 - A) * S[5]);

      
  }


  printRAW();  //Muestro por pantalla las lecturas promediadas
}
