import serial

arduino_port = "COM3"
baud = 9600
fileName = "acel_data.csv"

ser = serial.Serial(arduino_port, baud)
print("Conectado al puerto serial:" + arduino_port)
file = open(fileName, "a")
print("Archivo creado")

#muestre los datos en el terminal
getData = str (ser.readline ())
data = getData [0:] [: - 2]
print(data)

#add los datos al archivo
file = open (fileName, "a") #anexar los datos al archivo
file.write (data + "\\ n") #escribe datos con una nueva línea

print("Recoleccion de datos completa")
file.close()
